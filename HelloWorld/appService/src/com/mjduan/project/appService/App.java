package com.mjduan.project.appService;

import com.mjduan.project.orderService.OrderService;
import com.mjduan.project.userService.entity.User;
import com.mjduan.project.userService.service.UserService;

import java.util.Arrays;

public class App {

    public static void main(String[] args) {
        OrderService o = new OrderService();
        double price = o.getAllPrice(Arrays.asList(1, 2, 3));
        System.out.println(price);

        UserService userService = new UserService();
        User user = userService.getUser(1);
        System.out.println(user);

        userService.getUsers().forEach(System.out::println);
    }

}
