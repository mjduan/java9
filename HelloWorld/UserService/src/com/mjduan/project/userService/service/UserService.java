package com.mjduan.project.userService.service;

import com.mjduan.project.userService.entity.User;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class UserService {
    private static final Logger LOGGER = Logger.getLogger(UserService.class.getName());
    private static final Map<Integer, User> users = Map.ofEntries(
            Map.entry(1, new User("user1", 1, 12)),
            Map.entry(2, new User("user2", 2, 13)),
            Map.entry(3, new User("user3", 3, 14)),
            Map.entry(4, new User("user4", 4, 15)));

    public User getUser(int id) {
        LOGGER.info("Id is "+id);
        return users.get(id);
    }

    public List<User> getUsers() {
        LOGGER.info("query all users");
        return users.values().stream().collect(Collectors.toList());
    }
}
