module UserService {
    requires java.logging;

    exports com.mjduan.project.userService.entity;
    exports com.mjduan.project.userService;
    exports com.mjduan.project.userService.service;
}