package com.mjduan.project.orderService;

import com.mjduan.project.itemService.ItemService;

import java.util.List;
import java.util.function.BinaryOperator;
import java.util.logging.Logger;

public class OrderService {
    private static final Logger LOGGER = Logger.getLogger(OrderService.class.getName());
    private ItemService itemService = new ItemService();

    public double getAllPrice(List<Integer> items) {
        LOGGER.info(OrderService.class.getName() + " getAllPrice() items=" + items);
        double total = 0;
        total = items.stream()
                .map(integer -> itemService.getItem(integer).getPrice())
                .reduce(0.0, new BinaryOperator<Double>() {
                    @Override
                    public Double apply(Double aDouble, Double aDouble2) {
                        return aDouble + aDouble2;
                    }
                });
        return total;
    }

}
