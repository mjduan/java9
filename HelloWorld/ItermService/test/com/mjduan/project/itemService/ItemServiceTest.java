package com.mjduan.project.itemService;

import com.mjduan.project.itemService.entity.Item;
import org.junit.jupiter.api.Test;

public class ItemServiceTest {

    public static void main(String[] args){
        new ItemServiceTest().test();
    }

    public void test(){
        ItemService itemService = new ItemService();
        Item item = itemService.getItem(1);
        System.out.println(item.getId()+" "+item.getPrice()+" "+item.getName());
    }

}
