package com.mjduan.project.itemService;

import com.mjduan.project.itemService.entity.Item;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ItemService {
    private static final Logger LOGGER = Logger.getLogger(ItemService.class.getName());
    private static Map<Integer,Item> items = Map.ofEntries(
            Map.entry(1,new Item("item1",12.5,1)),
            Map.entry(2,new Item("item2",10.5,2)),
            Map.entry(3,new Item("item3",20.5,3)));

    public Item getItem(int id){
        LOGGER.info(ItemService.class.getName()+" getItem() id="+id);
        return items.get(id);
    }

    public List<Item> getItems(){
        LOGGER.info(ItemService.class.getName()+" getItems()");
        return items.values().stream().collect(Collectors.toList());
    }

}
