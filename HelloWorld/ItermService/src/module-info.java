module ItermService {
    requires java.logging;

    exports com.mjduan.project.itemService to OrderService;
    exports com.mjduan.project.itemService.entity to OrderService;
}